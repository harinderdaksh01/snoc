
import React, {createContext, useContext, useEffect, useState} from 'react';
import axios from "axios";
import {CUSTOMERS_PATH} from "../../components/API/customers/PATH";

const agreementContext = createContext()
export const useAgreementMasterContext = () => {
    return useContext(agreementContext);
};

const addUserInitialState = {
    agreement_code: {
        type: "text",
        name: "agreement_code",
        value: "",
        required: true,
        label: "Agreement Code",
    },
    customer_name: {
        type: "email",
        name: "customer_name",
        value: "",
        required: true,
        label: "Customer Name",
    },

    start_date: {
        type: "date",
        name: "start_date",
        value: "",
        required: true,
        label: "Start Date",
    },
    end_date: {
        type: "date",
        name: "end_date",
        value: "",
        required: true,
        label: "End Date",
    },
}

const MasterDataProvider = ({children}) => {
    const [userData, setUserData] = useState([])
    const [state,setState] = useState(addUserInitialState)
    const [agreeModal,setAgreeModal] = useState(false)

    const fetchAgreementData = () => {
        try {
            axios.get(`https://65b160f8d16d31d11bdeceab.mockapi.io/agreement`)
                .then((res) => {
                    setUserData(res.data)
                })
        } catch (e) {
            console.log(e)
        } finally {
        }
    }
    const addAgreementHandle = () => {
        const payload={
            agreement_code:state.agreement_code.value,
            customer_name: state.customer_name.value,
            start_date: state.start_date.value,
            end_date: state.end_date.value,
        }
        try {
            axios.post("https://65b160f8d16d31d11bdeceab.mockapi.io/agreement",payload )
                .then((res) => {
                    fetchAgreementData()
                    setAgreeModal(false)
                    setState(addUserInitialState)
                })
        } catch (e) {
            console.log(e)
        } finally {
            // setLoading(true)
        }
    }
    useEffect(() => {
        fetchAgreementData()
    }, [])
    const handleChange = (name, value) => {
        setState((_state) => {
            return {
                ..._state,
                [name]: {
                    ..._state[name],
                    value,
                },
            };
        });
    };

    return (
        <agreementContext.Provider value={{
            state,
            userData,
            agreeModal,
            setAgreeModal,
            addAgreementHandle,
            handleChange,
            fetchAgreementData
        }}>
            {children}
        </agreementContext.Provider>
    );
};

export default MasterDataProvider;
