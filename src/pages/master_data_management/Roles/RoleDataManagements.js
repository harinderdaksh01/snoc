import React, {useState} from 'react';
import {Box, Button, Typography} from "@mui/material";
import RoleDataProvider, {useMasterRoleContext} from "./RoleDataProvider";
import MainCard from "../../../layout/MainCard";
import DataTableLayout from "../../../layout/Table/DataTableLayout";
import AddMasterRole from "./AddMasterRole";

const RoleDataManagements = () => {
 const {masterRoleData,setRoleModal}=useMasterRoleContext()

    const columns = [
        {field: 'role_name', headerName: 'Role Name ', width: 400},
        {field: 'customer_name', headerName: 'Customer Name',  width: 200},
    ];

    return (
        <MainCard>
            <AddMasterRole />
            <Box display="flex" justifyContent={"end"} m={1}>
                <Button variant="contained" style={{background: "#5c8a2d"}} onClick={()=>setRoleModal(true)}>Add +</Button>
            </Box>
            <Box display="flex" justifyContent={"space-between"}>
                <Typography variant="h2" component="h2">
                    Roles
                </Typography>
                <Box display="flex" justifyContent={"space-between"}>
                </Box>
            </Box>
            <Box display="grid" gap={2} mt={3}>
                <DataTableLayout columns={columns} rows={masterRoleData}/>
            </Box>

        </MainCard>
    );
};

export default (props) => (
    <RoleDataProvider>
        <RoleDataManagements {...props} />
    </RoleDataProvider>
)

