
import React, {createContext, useContext, useEffect, useState} from 'react';
import axios from "axios";

const masterRoleContext = createContext()

export const useMasterRoleContext = () => {
    return useContext(masterRoleContext);
};
const initialRoleState = {
    role_name: {
        type: "text",
        name: "role_name",
        value: "",
        required: true,
        label: "Role Name",
    },
    customer_name: {
        type: "text",
        name: "customer_name",
        value: "",
        required: true,
        label: "Customer Name ",
    },
}
const RoleDataProvider = ({children}) => {
    const [state,setState] = useState(initialRoleState)
const [roleModal,setRoleModal] = useState(false)
    const [masterRoleData,setMasterRoleData] = useState([])
    const fetchMasterRoleData = () => {
        try {
            axios.get(`https://65b160f8d16d31d11bdeceab.mockapi.io/master-role`)
                .then((res) => {
                    setMasterRoleData(res.data)
                })
        } catch (e) {
            console.log(e)
        } finally {
        }
    }
    const addMasterRoleHandle = () => {
        const payload={
            role_name:state.role_name.value,
            customer_name: state.customer_name.value,
        }
        try {
            axios.post("https://65b160f8d16d31d11bdeceab.mockapi.io/master-role",payload )
                .then((res) => {
                    fetchMasterRoleData()
                    setRoleModal(false)
                    setState(initialRoleState)
                })
        } catch (e) {
            console.log(e)
        } finally {
            // setLoading(true)
        }
    }
    useEffect(() => {
        fetchMasterRoleData()
    }, [])
    const handleChange = (name, value) => {
        setState((_state) => {
            return {
                ..._state,
                [name]: {
                    ..._state[name],
                    value,
                },
            };
        });
    };
    return (
        <masterRoleContext.Provider value={{
            state,
            masterRoleData,
            roleModal,
            setRoleModal,
            handleChange,
            addMasterRoleHandle
        }}>
            {children}
        </masterRoleContext.Provider>
    );
};

export default RoleDataProvider;
