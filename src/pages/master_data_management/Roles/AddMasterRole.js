import React from 'react';
import {useMasterRoleContext} from "./RoleDataProvider";
import CommonModal from "../../../layout/CommonModal/CommonModal";
import {Grid} from "@mui/material";
import TextField from "../../../layout/Form/InputField";

const AddMasterRole = () => {
    const {state,addMasterRoleHandle, roleModal, setRoleModal,handleChange}=useMasterRoleContext()

    return (

        <CommonModal
            open={roleModal}
            addCustomerHandle={addMasterRoleHandle}
            onHandleClose={()=>setRoleModal(false)}
            title={"Add Customer"}
            btnTxt={"Add"}
        >
            <form action="">

                <Grid container spacing={2} alignItems="center">
                    <Grid item xs={6} mb={2}>
                        <TextField {...state.role_name} onChange={handleChange}/>
                    </Grid>
                    <Grid item xs={6} mb={2}>
                        <TextField {...state.customer_name} onChange={handleChange}/>
                    </Grid>
                </Grid>



            </form>

        </CommonModal>
    );
};

export default AddMasterRole;
