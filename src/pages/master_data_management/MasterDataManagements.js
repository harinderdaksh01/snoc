import React, {useState} from 'react';
import MainCard from "../../layout/MainCard";
import {Box, Button, Typography} from "@mui/material";
import DataTableLayout from "../../layout/Table/DataTableLayout";
import {DeleteOutlined} from "@ant-design/icons";
import MasterDataProvider, {useAgreementMasterContext} from "./MasterDataProvider";
import EndAgreement from "./EndAgreement";
import AddAgreementData from "./AddAgreementData";

const MasterDataManagements = () => {
    const { userData,setAgreeModal} = useAgreementMasterContext()
    const columns = [
        {field: 'agreement_code', headerName: 'Agreement Code', flex: 1},
        {field: 'customer_name', headerName: 'Customer Name', flex: 1},
        {field: 'start_date', headerName: 'Start date', flex: 1,},
        {field: 'end_date', headerName: 'End date ', flex: 1},
        {field: '', headerName: 'Actions', flex: 1,
            renderCell: (params) => {
                return (
                    <>
                        <EndAgreement params={params.row}/>
                    </>
                )
            }
        },
    ];



    return (
        <MainCard>
            <AddAgreementData />
            <Box display="flex" justifyContent={"end"} m={1}>
                <Button variant="contained" style={{background: "#5c8a2d"}} onClick={()=>setAgreeModal(true)}>Add +</Button>
            </Box>
            <Box display="flex" justifyContent={"space-between"}>
                <Typography variant="h2" component="h2">
                    Agreement Data
                </Typography>
                <Box display="flex" justifyContent={"space-between"}>
                </Box>
            </Box>
            <Box display="grid" gap={2} mt={3}>
                <DataTableLayout

                    style={{width:"100%"}}
                    columns={columns} rows={userData}/>
            </Box>

        </MainCard>
    );
};

export default (props) => (
    <MasterDataProvider>
        <MasterDataManagements {...props} />
    </MasterDataProvider>
)

