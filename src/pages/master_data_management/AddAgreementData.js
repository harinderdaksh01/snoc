import React from 'react';
import {Box, Button, Grid} from "@mui/material";
import CommonModal from "../../layout/CommonModal/CommonModal";
import TextField from "../../layout/Form/InputField";
import {useAgreementMasterContext} from "./MasterDataProvider";
const AddAgreementData = () => {
    const {state,agreeModal,setAgreeModal, addAgreementHandle,handleChange} = useAgreementMasterContext()

    return (
        <>

            <CommonModal
                open={agreeModal}
                addCustomerHandle={addAgreementHandle}
                onHandleClose={()=>setAgreeModal(false)}
                title={"Add Customer"}
                btnTxt={"Add"}
            >
                <form action="">

                    <Grid container spacing={2} alignItems="center">
                        <Grid item xs={6} mb={2}>
                            <TextField {...state.agreement_code} onChange={handleChange}/>
                        </Grid>
                        <Grid item xs={6} mb={2}>
                            <TextField {...state.customer_name} onChange={handleChange}/>
                        </Grid>
                    </Grid>

                    <Grid container spacing={2} alignItems="center">
                        <Grid item xs={6} mb={2}>
                            <TextField {...state.start_date} onChange={handleChange}/>
                        </Grid>
                        <Grid item xs={6} mb={2}>
                            <TextField {...state.end_date} onChange={handleChange}/>
                        </Grid>
                    </Grid>

                </form>

            </CommonModal>
        </>
    );
};

export default AddAgreementData;
