import React, {useState} from 'react';
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import axios from "axios";
import {CUSTOMERS_PATH} from "../../components/API/customers/PATH";
import {useAgreementMasterContext} from "./MasterDataProvider";

const EndAgreement = ({params}) => {
    const {id} = params
    const [open,setOpen] = useState(false)
    const [btnDisable,setBtnDisable] =useState(false)
    const {fetchAgreementData} = useAgreementMasterContext()

    const handleEndAgreement = () => {
        setBtnDisable(true)
        try {
            axios.delete(`https://65b160f8d16d31d11bdeceab.mockapi.io/agreement/${id}`)
                .then((res) => {
                    fetchAgreementData()
                })
        } catch (e) {
            // setError(e)
            console.log(e)
        } finally {
            // setLoading(false)
        }
    }
    return (
        <div>
            <Button onClick={()=>setOpen(true)} variant="contained" style={{background: "#5c8a2d"}}> End Agreement</Button>
            <Dialog
                open={open}
                // TransitionComponent={Transition}
                keepMounted
                onClose={()=>setOpen(true)}
            >
                <DialogTitle>Confirm End Agreement</DialogTitle>
                <DialogContent>
                    <DialogContentText >
                        Are you sure you want to End this Agreement?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" onClick={()=>setOpen(false)}>cancel</Button>
                    <Button
                        disabled={btnDisable}
                        color="error" variant="contained" onClick={handleEndAgreement}>End</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default EndAgreement;
