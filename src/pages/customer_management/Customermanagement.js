import React, {useEffect, useState} from 'react';
import DataTableLayout from "../../layout/Table/DataTableLayout";
import {Box, Grid, Button, Paper, Typography} from "@mui/material";
import MainCard from "../../layout/MainCard";
import AddCustomer from "./AddCustomer";
import {DeleteFilled, DeleteOutlined, EditOutlined} from "@ant-design/icons";
import EditCustomers from "./EditCustomers";
import axios from "axios";
import {CUSTOMERS_PATH} from "../../components/API/customers/PATH";
import ConfirmationDialog from "../../components/Dialog/ConfirmationDialog";
import DeleteCustomer from "./DeleteCustomer";

const Customermanagement = () => {

    const [loading, setLoading] = useState(false)
    const [error, setError] = useState('')
    const [customerData, setCustomerData] = useState([])
    const fetchcustomerData = () => {
        setLoading(true)
        try {
            axios.get(`${CUSTOMERS_PATH}/user-managent`)
                .then((res) => {
                    setCustomerData(res.data)
                })
        } catch (e) {
            setError(e)
        } finally {
            setLoading(false)
        }
    }
    useEffect(() => {
        fetchcustomerData()
    }, [])



    const columns = [
        {field: 'id', headerName: 'ID', flex: 1},
        {field: 'name', headerName: 'Name', flex: 1},
        {field: 'email', headerName: 'Official Email', flex: 1},
        {
            field: 'phone',
            headerName: 'Point of Contact',
            flex: 1,
        },
        {
            headerName: 'Edit',
            field: 'edit',
            width: 100,
            renderCell: (params) => {
                return (
                    <>
                        <EditCustomers params={params.row}

                        />

                    </>
                )
            }
        },
        {
            headerName: 'delete',
            field: 'delete',
            width: 100,
            renderCell: (params) => {
                return (
                    <>
<DeleteCustomer params={params.row}
                fetchcustomerData={fetchcustomerData}
/>
                    </>
                )
            }
        },
    ];
    return (
        <>
            <MainCard>
                <AddCustomer
                    fetchcustomerData={fetchcustomerData}
                />
                <Box display="flex" justifyContent={"space-between"}>
                    <Typography variant="h2" component="h2">
                        Customers
                    </Typography>
                    <Box display="flex" justifyContent={"space-between"}>
                    </Box>
                </Box>
                <Box display="grid" gap={2} mt={3}>
                    <DataTableLayout columns={columns} rows={customerData} loading={loading} error={error}/>
                </Box>

            </MainCard>
        </>
    )
        ;
};

export default Customermanagement;
