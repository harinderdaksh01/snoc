import React, {useState} from 'react';
import UserProvider, {useCreateContext} from "./UserProvider";
import MainCard from "../../layout/MainCard";
import AddCustomer from "../customer_management/AddCustomer";
import {Box, Button, Typography} from "@mui/material";
import DataTableLayout from "../../layout/Table/DataTableLayout";
import EditCustomers from "../customer_management/EditCustomers";
import EditUserRole from "./EditUserRole";
import {DeleteOutlined} from "@ant-design/icons";
import AddUser from "./AddUser";
import EditUserData from "./EditUserData";
import DeleteUser from "./DeleteUser";

const UserManagement = () => {

    const {userData,setUserModal} = useCreateContext()

    const columns = [
        {field: 'id', headerName: 'ID', flex: 1},
        {field: 'name', headerName: 'Name', flex: 1},
        {field: 'email', headerName: 'Email Id', flex: 1},
        {
            headerName: 'Role',
            field:'role',
            flex: 1,
            renderCell: (params) => {
                return (
                    <>
                        <EditUserRole params={params.row}/>

                    </>
                )
            }
        },
        {
            field: '', headerName: 'Actions', flex: 1,
            renderCell: (params) => {
                return (
                    <>
                        <EditUserData params={params} />
<DeleteUser params={params} />
                    </>
                )
            }
        },


    ];

    return (
        <MainCard>
            <AddUser />
            <Box display="flex" justifyContent={"end"} m={1}>
                <Button variant="contained" style={{background: "#5c8a2d"}} onClick={()=>setUserModal(true)}>Add +</Button>
            </Box>
            <Box display="flex" justifyContent={"space-between"}>
                <Typography variant="h2" component="h2">
                    User
                </Typography>
                <Box display="flex" justifyContent={"space-between"}>
                </Box>
            </Box>
            <Box display="grid" gap={2} mt={3}>
                <DataTableLayout columns={columns} rows={userData}/>
            </Box>
        </MainCard>
    );
};

export default (props) => (
    <UserProvider>
        <UserManagement {...props} />
    </UserProvider>
)
