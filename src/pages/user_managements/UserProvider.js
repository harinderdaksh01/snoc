import React, {createContext, useContext, useEffect, useState} from 'react';
import axios from "axios";
import {CUSTOMERS_PATH} from "../../components/API/customers/PATH";

const createUserContext = createContext()

const addUserInitialState = {
    name: {
        type: "text",
        name: "name",
        value: "",
        required: true,
        label: "Name",
    },
    email: {
        type: "email",
        name: "email",
        value: "",
        required: true,
        label: "Official Email",
    },
}

export const useCreateContext = () => {
    return useContext(createUserContext);
};

const UserProvider = ({children}) => {
    const [userData, setUserData] = useState([])
    const [state,setState] = useState(addUserInitialState)
    const [userModal,setUserModal] = useState(false)

    const fetchUserData = () => {
        try {
            axios.get(`${CUSTOMERS_PATH}/user-management-data`)
                .then((res) => {
                    setUserData(res.data)
                })
        } catch (e) {
         console.log(e)
        } finally {
        }
    }
    const addUserHandle = () => {
        const payload={
    name:state.name.value,
            email: state.email.value
}
        try {
            axios.post("https://65acf49dadbd5aa31bdfd287.mockapi.io/user-management-data",payload )
                .then((res) => {
                    fetchUserData()
                    setUserModal(false)
                    setState(addUserInitialState)
                })
        } catch (e) {
            console.log(e)
        } finally {
            // setLoading(true)
        }
    }
    useEffect(() => {
        fetchUserData()
    }, [])

    const handleChange = (name, value) => {
        setState((_state) => {
            return {
                ..._state,
                [name]: {
                    ..._state[name],
                    value,
                },
            };
        });
    };


    return (
        <createUserContext.Provider value={{
            state,
            userData,
            addUserHandle,
            addUserInitialState,
            handleChange,
            userModal,
            fetchUserData,
            setUserModal
        }}
        >
            {children}
        </createUserContext.Provider>
    );
};

export default UserProvider;
