import React, {useState} from 'react';
import {DeleteFilled} from "@ant-design/icons";
import ConfirmationDialog from "../../components/Dialog/ConfirmationDialog";
import axios from "axios";
import {CUSTOMERS_PATH} from "../../components/API/customers/PATH";
import {useCreateContext} from "./UserProvider";

const DeleteUser = ({params}) => {
    const [open, setOpen] = useState(false)
    const [btnDisable,setBtnDisable] = useState(false)
    const {fetchUserData} = useCreateContext()
    const {id} = params
    const handleClose = () => {
        setOpen(false)
    }
    const handleCustomerDelete = () => {
        setBtnDisable(true)
        try {
            axios.delete(`${CUSTOMERS_PATH}/user-management-data/${id}`)
                .then((res) => {
                    fetchUserData()
                })
        } catch (e) {
            // setError(e)
            console.log(e)
        } finally {
            // setLoading(false)
        }
    }
    return (
        <div>
            <DeleteFilled
                onClick={() => setOpen(true)}
                style={{cursor: "pointer"}}
            />
            <ConfirmationDialog
                btnDisable={btnDisable}
                open={open}
                handleDelete={handleCustomerDelete}
                handleClose={handleClose}
            />
        </div>
    );
};

export default DeleteUser;
