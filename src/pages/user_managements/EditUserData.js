import React, {useState} from 'react';
import CommonModal from "../../layout/CommonModal/CommonModal";
import {Button, Grid} from "@mui/material";
import TextField from "../../layout/Form/InputField";
import {useCreateContext} from "./UserProvider";
import {EditFilled} from "@ant-design/icons";
import axios from "axios";
import {CUSTOMERS_PATH} from "../../components/API/customers/PATH";
const initialData = {
    name: {
        type: "text",
        name: "name",
        value: "",
        required: true,
        label: "Name",
    },
    email: {
        type: "email",
        name: "email",
        value: "",
        required: true,
        label: "Official Email",
    },
    phone: {
        type: "text",
        name: "phone",
        value: "",
        required: true,
        label: "Point of Contact",
    },
}
const EditUserData = ({params}) => {
    console.log("edit user data",params)
    const [open, setOpen] = useState(false)
    const [data,setData] = useState(initialData)
    const {fetchUserData} = useCreateContext()

    const EditUserHandle = () =>{
        const payload = {
            name:data.name.value,
            email:data.email.value,
        }
    try {
        axios.put(`${CUSTOMERS_PATH}/user-management-data/${params.id}`,payload)
            .then((res) => {
                setOpen(false)
                fetchUserData()
            })
    } catch (e) {
        console.log(e)
    } finally {
    }
}
    const handleChange = (name, value) => {
        setData((_state) => {
            return {
                ..._state,
                [name]: {
                    ..._state[name],
                    value,
                },
            };
        });
    };
    return (
        <>
            <Button onClick={() => setOpen(true)}><EditFilled
                style={{color: "black", marginRight: "auto", fontSize: "20px"}}/></Button>

            <CommonModal
                open={open}
                addCustomerHandle={EditUserHandle}
                onHandleClose={() => setOpen(false)}
                title={"Edit User"}
                btnTxt={"Save"}
            >
                <form action="">
                    <Grid container spacing={2} alignItems="center">
                        <Grid item xs={6} mb={2}>
                            <TextField {...data.name} onChange={handleChange}/>
                        </Grid>
                        <Grid item xs={6} mb={2}>
                            <TextField {...data.email} onChange={handleChange}/>
                        </Grid>
                    </Grid>

                </form>

            </CommonModal>
        </>

    );
};

export default EditUserData;
