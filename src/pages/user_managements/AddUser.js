import React from 'react';
import {Box, Button, Grid} from "@mui/material";
import CommonModal from "../../layout/CommonModal/CommonModal";
import TextField from "../../layout/Form/InputField";
import {useCreateContext} from "./UserProvider";

const AddUser = () => {
    const {state,userModal,addUserHandle,handleChange,setUserModal} = useCreateContext()
    return (
        <>

            <CommonModal
                open={userModal}
                addCustomerHandle={addUserHandle}
                onHandleClose={()=>setUserModal(false)}
                title={"Add Customer"}
                btnTxt={"Add"}
            >
                <form action="">

                    <Grid container spacing={2} alignItems="center">
                        <Grid item xs={6} mb={2}>
                            <TextField {...state.name} onChange={handleChange}/>
                        </Grid>
                        <Grid item xs={6} mb={2}>
                            <TextField {...state.email} onChange={handleChange}/>
                        </Grid>
                    </Grid>

                </form>

            </CommonModal>
        </>
    );
};

export default AddUser;
