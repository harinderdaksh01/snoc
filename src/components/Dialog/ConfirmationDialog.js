import React from 'react';
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";

const ConfirmationDialog = ({open,handleClose,handleDelete,btnDisable}) => {
    return (
        <Dialog
            open={open}
            // TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
        >
            <DialogTitle>Confirm Delete</DialogTitle>
            <DialogContent>
                <DialogContentText >
                    Are you sure you want to delete this item?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" onClick={handleClose}>cancel</Button>
                <Button
                    disabled={btnDisable}
                    color="error" variant="contained" onClick={handleDelete}>Delete</Button>
            </DialogActions>
        </Dialog>
)
    ;
};

export default ConfirmationDialog;
